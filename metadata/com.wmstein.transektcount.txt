Categories:Science & Education
License:Apache 2.0
Web Site:
Source Code:https://github.com/wistein/TransektCount
Issue Tracker:https://github.com/wistein/TransektCount/issues

Summary:Transect counting according to the Butterfly Monitoring Scheme methodology
Description:
TransektCount is an Android App that supports transect counters in nature
preserving projects according to the Butterfly Monitoring Scheme methodology. It
allows a species-specific counting per transect section.

The integrated database is organized according to a single transect inspection.
That means, a new (prepared and importable) basic database instance will be used
per inspection.
.

Repo Type:git
Repo:https://github.com/wistein/TransektCount.git

Build:1.3.1,131
    commit=723ce09e029c52d1bb869538e0bd00488bfc9379
    subdir=transektcount
    gradle=yes

Auto Update Mode:None
Update Check Mode:None
